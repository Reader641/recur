CPPFLAGS=-std=c++17 -Wall 
CC=g++
CFLGS=--use-colour yes
CATCH2FLAGS=--success --use-colour yes
TESTS=[tests]
TEST1=[test1]
TEST2=[test2]
TEST3=[test3]
TEST4=[test4]
TEST5=[test5]
TEST6=[test6]
TEST7=[test7]
TEST8=[test8]
TEST9=[test9]
TEST10=[test10]
TEST11=[test11]
TEST12=[test12]



tests:  bin/tests.out 
	@bin/tests.out $(C2FLGS)

test1: bin/tests.out 
	@bin/tests.out $(TEST1) $(C2FLGS)

test2: bin/tests.out 
	@bin/tests.out $(TEST2) $(C2FLGS)

test3: bin/tests.out 
	@bin/tests.out $(TEST3) $(C2FLGS)

test4: bin/tests.out
	@bin/tests.out $(TEST4) $(C2FLGS)

test5: bin/tests.out
	@bin/tests.out $(TEST5) $(C2FLGS)

test6: bin/tests.out
	@bin/tests.out $(TEST6) $(C2FLGS)

test7: bin/tests.out
	@bin/tests.out $(TEST7) $(C2FLGS)

test8: bin/tests.out
	@bin/tests.out $(TEST8) $(C2FLGS)

test9: bin/tests.out
	@bin/tests.out $(TEST9) $(C2FLGS)

test10: bin/tests.out
	@bin/tests.out $(TEST10) $(C2FLGS)

test11: bin/tests.out
	@bin/tests.out $(TEST11) $(C2FLGS)

test12: bin/tests.out
	@bin/tests.out $(TEST12) $(C2FLGS)


all: src/main.cpp
	@printf "Compiling custom main file...\n"       
	$(CC) $(CPPFLAGS) -o bin/main.out src/main.cpp

run:
	@bin/main.out



bin/tests.out: bin/tests_main.o tests/tests.cpp tests/tests_main.cpp src/modules.hpp
	@printf "\033[36mCompiling Tests...\n\033[0m"	
	$(CC) $(CPPFLAGS)  -o bin/tests.out bin/tests_main.o tests/tests.cpp

bin/tests_main.o: tests/tests_main.cpp 
	@printf "\033[36mCompiling Test Driver...(please be patient)\n\033[0m"	
	$(CC) $(CPPFLAGS)  -c tests/tests_main.cpp -o bin/tests_main.o


clean:
	@printf "\033[31mRemoving objects (and temporary files)\033[0m\n"
	@rm -rf bin/*.o*

