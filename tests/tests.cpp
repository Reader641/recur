#include "catch.hpp"
#include "../src/modules.hpp"

// Some basic test cases

TEST_CASE("Testing both positive","[test1]")
{
  CHECK(product(2,5) == 10);
}


TEST_CASE("Testing both positive ","[test2]")
{
  CHECK(product(12,15) == 180);
}


TEST_CASE("Testing first negative","[test3]")
{
  CHECK(product(-6,7) == -42);
}


TEST_CASE("Testing first negative ","[test4]")
{
  CHECK(product(-13,27) == -351);
}

TEST_CASE("Testing second negative","[test5]")
{
  CHECK(product(3,-5) == -15);
}

TEST_CASE("Testing second negative ","[test6]")
{
  CHECK(product(23,-15) == -345);
}

TEST_CASE("Testing both negative","[test7]")
{
  CHECK(product(-6,-5) == 30);
}

TEST_CASE("Testing both negative ","[test8]")
{
  CHECK(product(-25,-25) == 625);
}

TEST_CASE("Testing zero","[test9]")
{
  CHECK(product(0,-25) == 0);
}

TEST_CASE("Testing zero ","[test10]")
{
  CHECK(product(160,0) == 0);
}

TEST_CASE(" Testing zero","[test11]")
{
  CHECK(product(0,5) == 0);
}


TEST_CASE("Testing  zero","[test12]")
{
  CHECK(product(0,0) == 0);
}
