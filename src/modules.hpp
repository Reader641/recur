int  product(int a, int b)
{
	if (a > b)
	{
		return product(b, a);
	}
	
	if (a > 0 && b < 0)
	{
		return product(b, a);
	}
	else if (a < 0 && b < 0)
	{
		return product (-(a),-(b));
	}
	
	if (b != 0) 
	{
		return (a + product(a, b -1));
	}
	else
	{
		return 0;
	}
	
}
